ALL: tictactoe sfml-tictactoe tests

SFML = `pkg-config --libs --cflags sfml-graphics`

headers = board.hpp player.hpp

tictactoe: main.cpp $(headers)
	$(CXX) -o $@ $< $(LDFLAGS)

sfml-tictactoe: sfml-tictactoe.cpp $(headers)
	$(CXX) -o $@ $< $(SFML) $(LDFLAGS)

tests: tests.cpp $(headers)
	$(CXX) -o $@ $< $(LDFLAGS) -lboost_unit_test_framework

test: tests
	./tests

clean:
	-rm tictactoe
	-rm tests

.PHONY: clean