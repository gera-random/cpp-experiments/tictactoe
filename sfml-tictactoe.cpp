#include <SFML/Window/Event.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <cmath>

#include "board.hpp"
#include "player.hpp"

const int WW = 500;
const int WH = 500;

void drawGrid(sf::RenderWindow &window, int thickness)
{
	sf::RectangleShape line;
	line.setFillColor(sf::Color(128, 128, 128));
	line.setSize(sf::Vector2<float>(WW, thickness));
	line.setOrigin(0, thickness/2);
	line.setPosition(0, WH/3.0f);
	window.draw(line);
	line.setPosition(0, WH*2.0f/3.0f);
	window.draw(line);
	line.setSize(sf::Vector2<float>(thickness, WH));
	line.setOrigin(thickness/2, 0);
	line.setPosition(WW/3.0f, 0);
	window.draw(line);
	line.setPosition(WW*2.0f/3.0f, 0);
	window.draw(line);
}

float getOx(int thickness, int position)
{
	float cellW = (WW-2*thickness)/3.0f;
	int col = (position-1) % 3;
	return cellW * col + thickness*col + cellW/2;
}

float getOy(int thickness, int position)
{
	float cellH = (WH-2*thickness)/3.0f;
	int row = (position-1) / 3;
	return cellH * row + thickness*row + cellH/2;
}

void drawX(sf::RenderWindow &window, int width, int thickness, int position)
{
	sf::RectangleShape line;
	line.setFillColor(sf::Color(255, 0, 0));

	float cellW = (WW-2*thickness)/3.0f;
	float cellH = (WH-2*thickness)/3.0f;
	float angle = atanf(cellH/cellW)*180.f/M_PI;
	line.setOrigin(width/2, thickness/2);

	line.setPosition(getOx(thickness, position), getOy(thickness, position));
	line.setSize(sf::Vector2<float>(width, thickness));
	line.rotate(angle);
	window.draw(line);
	line.rotate(-2*angle);
	window.draw(line);
}

void drawO(sf::RenderWindow &window, int diameter, int thickness, int position)
{
	sf::CircleShape circle;
	circle.setFillColor(sf::Color(0, 0, 0));
	circle.setOutlineColor(sf::Color(0, 0, 255));
	circle.setOutlineThickness(thickness);
	circle.setRadius(diameter/2.0f);

	circle.setOrigin(diameter/2, diameter/2);
	circle.setPosition(getOx(thickness, position), getOy(thickness, position));
	window.draw(circle);
}

void drawGame(sf::RenderWindow &window, Board &b)
{
	window.clear(sf::Color::Black);
	drawGrid(window, 10);
	for (int i = 1; i <= 9; ++i) {
		if (b.get(i) == X)
			drawX(window, 100, 10, i);
		if (b.get(i) == O)
			drawO(window, 80, 10, i);
	}
	window.display();
}

int getBoardPosition(int x, int y)
{
	float cellW = WW/3.0f;
	float cellH = WH/3.0f;

	int bx = (int)floorf(x/cellW) + 1;
	int by = (int)floorf(y/cellH);

	return by * 3 + bx;
}

bool playerInput(sf::RenderWindow &window, float x, float y, Board &b)
{
	float adjX = x * WW / window.getSize().x;
	float adjY = y * WH / window.getSize().y;
	int pos = getBoardPosition(adjX, adjY);
	try {
	b.put(X, pos);
	return true;
	} catch (const std::logic_error& e) {
		std::cout << "logic error\n";
		return false;
	} catch (const std::range_error& e) {
		std::cout << "range error\n";
		return false;
	}
}

int main()
{
	sf::RenderWindow window(sf::VideoMode(WW, WH), "Tic-Tac-Toe");

	Board b;
	MinimaxPlayer opponent(b, O);
	bool gameOver = false;
	
	drawGame(window, b);
	while (window.isOpen()) {
		sf::Event event;
		if (window.waitEvent(event)) {
			if (event.type == sf::Event::Closed) {
				window.close();
			}
			if (event.type == sf::Event::Resized) {
				drawGame(window, b);
			}
			if (event.type == sf::Event::MouseButtonPressed) {
				if (event.mouseButton.button == sf::Mouse::Left) {
					if (!gameOver) {
						if (!playerInput(window, event.mouseButton.x, event.mouseButton.y, b)) {
							continue;
						}
						drawGame(window, b);
					}
					gameOver = b.isOver();
					if (!gameOver) {
						opponent.makeMove();
					}
					gameOver = b.isOver();
				} else if (event.mouseButton.button == sf::Mouse::Right) {
					if (gameOver) b.reset();
					gameOver = false;
				}
				drawGame(window, b);
			}
		} else break;
	}
	return 0;
}