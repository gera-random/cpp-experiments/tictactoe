#pragma once
#include <random>

#include "board.hpp"

class Player
{
public:
	Player(Board& board, Mark mark) : b(board), mark(mark) {}

	virtual void makeMove() = 0;

	Mark getMark() {return mark;}

protected:
	Board& b;
	Mark mark;
};

class HumanPlayer : public Player
{
public:
	using Player::Player;

	void makeMove() override
	{
		std::cout << (char)mark << " move [1-9] ";
		int n;
		std::cin >> n;
		b.put(mark, n);
	}
};

class RandomPlayer : public Player
{
public:
	RandomPlayer(Board &b, Mark m, unsigned int seed) : Player(b,m), dist(1,9), rng(seed)
	{}

	void makeMove() override
	{
		while (true) {
			int rn = dist(rng);
			if (b.get(rn) == EMPTY) {
				b.put(mark, rn);
				break;
			}
		}
	}

private:
	std::mt19937 rng;
	std::uniform_int_distribution<> dist;
};

class MinimaxPlayer : public Player
{
public:
	MinimaxPlayer(Board& b, Mark m) : Player(b,m)
	{
		if (m == O) opponent = X;
		else opponent = O;
	}

	void makeMove() override
	{
		minimax(b, 0, true);
		b.put(mark, nextMove);
	}

private:
	Mark opponent;
	int nextMove = 0;

	int score(Board& b, int depth)
	{
		if (b.isVictory(mark))
			return 10 - depth;
		if (b.isVictory(opponent))
			return depth - 10;
		return 0;
	}

	int minimax(Board board, int depth, bool myMove)
	{
		if (board.isOver()) return score(board, depth);
		++depth;

		int bestNextMove;
		int bestScore = -100;
		if (!myMove) bestScore = 100;

		for (int i = 1; i <= 9; ++i) {
			if (board.get(i) == EMPTY) {
				Board nextState = board;

				if (myMove)
					nextState.put(mark, i);
				else
					nextState.put(opponent, i);

				int nextScore = minimax(nextState, depth, !myMove);
				if (myMove && nextScore > bestScore ||
					!myMove && nextScore < bestScore) {
					bestScore = nextScore;
					bestNextMove = i;
				}
			}
		}
		nextMove = bestNextMove;
		return bestScore;
	}
};