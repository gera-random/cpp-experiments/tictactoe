#include <iostream>
#include <stdexcept>

#include "board.hpp"
#include "player.hpp"

int main()
{
	Board b;
	MinimaxPlayer p1(b, X);
	MinimaxPlayer p2(b, O);

	b.print();
	Player* p = &p1;
	while (true) {
		try {
			p->makeMove();
		} catch (const std::logic_error& e) {
			std::cout << "logic error\n";
			continue;
		} catch (const std::range_error& e) {
			std::cout << "range error\n";
			continue;
		}
		b.print();
		if (b.isVictory(p->getMark())) {
			std::cout << (char)p->getMark() << " player won\n";
			break;
		}
		if (b.isTie()) {
			std::cout << "Tie\n";
			break;
		}
		if (p == &p1) p = &p2;
		else if (p == &p2) p = &p1;
	}
	return 0;
}