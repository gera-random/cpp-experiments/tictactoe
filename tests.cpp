#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include "board.hpp"
#include "player.hpp"

BOOST_AUTO_TEST_CASE( test_victory1 )
{
	Board b;
	b.put(X,1);
	b.put(X,2);
	b.put(X,3);
	BOOST_CHECK( b.isVictory(X) );
	BOOST_CHECK( !b.isVictory(O) );
	BOOST_CHECK( !b.isTie() );
}

BOOST_AUTO_TEST_CASE( test_victory2 )
{
	Board b;
	b.put(O,9);
	b.put(O,6);
	b.put(O,3);
	BOOST_CHECK( !b.isVictory(X) );
	BOOST_CHECK( b.isVictory(O) );
	BOOST_CHECK( !b.isTie() );
}

BOOST_AUTO_TEST_CASE( test_victory3 )
{
	Board b;
	b.put(O,9);
	b.put(O,5);
	b.put(O,1);
	BOOST_CHECK( !b.isVictory(X) );
	BOOST_CHECK( b.isVictory(O) );
	BOOST_CHECK( !b.isTie() );
}


BOOST_AUTO_TEST_CASE( test_tie )
{
	Board b;
	b.put(X,5);
	b.put(O,6);
	b.put(X,9);
	b.put(O,1);
	b.put(X,4);
	b.put(O,8);
	b.put(X,7);
	b.put(O,3);
	b.put(X,2);

	BOOST_CHECK( !b.isVictory(X) );
	BOOST_CHECK( !b.isVictory(O) );
	BOOST_CHECK( b.isTie() );
}

BOOST_AUTO_TEST_CASE( test_minimax_players_tie )
{
	Board b;
	MinimaxPlayer p1(b, O);
	MinimaxPlayer p2(b, X);

	while (true) {
		p1.makeMove();
		if (b.isOver()) break;
		p2.makeMove();
		if (b.isOver()) break;
	}

	BOOST_CHECK( b.isTie() );
}

BOOST_AUTO_TEST_CASE( test_minimax_random_player_win_or_tie )
{
	Board b;
	MinimaxPlayer p1(b, O);
	std::random_device rd;
	RandomPlayer p2(b, X, rd());

	while (true) {
		p1.makeMove();
		if (b.isOver()) break;
		p2.makeMove();
		if (b.isOver()) break;
	}

	BOOST_CHECK( !b.isVictory(X) );
}

BOOST_AUTO_TEST_CASE( test_minimax_player_blocks_opponent_victory )
{
	Board b;
	MinimaxPlayer p1(b, O);

	b.put(X,2);
	b.put(O,7);
	b.put(X,6);
	b.put(O,8);
	b.put(X,9);

	p1.makeMove();

	BOOST_CHECK( b.get(3) == O );
}