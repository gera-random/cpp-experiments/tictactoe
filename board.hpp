#pragma once

#include <iostream>

enum Mark
{
	X = 'X',
	O = 'O',
	EMPTY = '-'
};

class Board
{
public:
	Board() {}
	Board(const Board& other) : m_data(other.m_data) {}

	void print()
	{
		std::cout << "\n";
		for (int i = 0; i < 9; ++i)
			std::cout << m_data[i] << " \n"[(i+1)%3==0];
		std::cout << "\n";
	}

	void reset()
	{
		m_data = "---------";
	}

	void put(Mark mark, int n)
	{
		check_range(n);
		if (m_data[n-1] != EMPTY)
			throw std::logic_error("cannot place mark: place is occupied");
		m_data[n-1] = mark;
	}

	char get(int n)
	{
		check_range(n);
		return m_data[n-1];
	}

	bool isOver()
	{
		return isVictory(X) || isVictory(O) || isTie();
	}

	bool isVictory(Mark mark)
	{
		bool victory = false;
		for (int i = 0; i < 3; ++i)
		{
			// horizontal
			victory |= ((get(3*i+1) == mark) && (get(3*i+2) == mark) && (get(3*i+3) == mark));
			// vertical
			victory |= ((get(1+i) == mark) && (get(4+i) == mark) && (get(7+i) == mark)); 
		}
		// diagonal
		victory |= ((get(1) == mark) && (get(5) == mark) && (get(9) == mark));
		victory |= ((get(3) == mark) && (get(5) == mark) && (get(7) == mark));
		return victory;
	}

	bool isTie()
	{
		bool tie = !isVictory(X) && !isVictory(O);
		for (int i = 0; i < 9; ++i)
			if (m_data[i] == EMPTY)
				tie = false;
		return tie;
	}

private:
	std::string m_data = "---------";

	void check_range(int n)
	{
		if (n < 1 || n > 9)
			throw std::range_error("referring to a place outside of the tic-tac-toe board");
	}
};